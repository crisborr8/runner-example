from flask import Blueprint, request, jsonify
from .geneartor import *
#########################################################
#########################################################
#########################################################
example_api = Blueprint('example_api', __name__, url_prefix='')
methods = ["DELETE", "GET", "POST", "PATCH", "PUT"]
#########################################################
#########################################################
#########################################################
@example_api.route("/get_array_list", methods=methods)
def get_array_list():
    errors = {}
    if request.method == "GET":
    #----------------------------------------------------
    #----------------------------------------------------
        array_elementos = []
        for _ in range(10):
            elemento = {
                "dato": generar_cadena_aleatoria(),
                "valor": generar_numero_aleatorio()
            }
            array_elementos.append(elemento)
        return jsonify(array_elementos), 200
        #------------------------------------------------
        #------------------------------------------------
    else:
        errors = {
            "msg": request.method + ' No implementado.',
        }
        return jsonify(errors), 501