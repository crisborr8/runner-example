# runner-example

## Pasos para ejecutar el repositorio
1. ```python3.11 -m venv venv```
2. ```source venv/bin/activate```
3. ```pip install -r requirements.txt```
4. ```cd src```
5. ```flask run```