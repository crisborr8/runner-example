from unittest.mock import patch
import json
#########################################################
#########################################################
#########################################################
def test_get_array_list_ok(client, app):
    with app.test_request_context():
        with patch('src.example_api.generar_cadena_aleatoria', return_value="asdfg"):
            with patch('src.example_api.generar_numero_aleatorio', return_value=1234):
                array_elementos_esperado = []
                for _ in range(10):
                    elemento = {
                        "dato": "asdfg",
                        "valor": 1234
                    }
                    array_elementos_esperado.append(elemento)
                headers = {}
                response = client.get('/get_array_list', headers=headers, json={})
                assert response.status_code == 200
                data = json.loads(response.data)
                assert data == array_elementos_esperado