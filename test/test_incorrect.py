import json
#########################################################
#########################################################
#########################################################
def test_get_array_list_ok(client, app):
    with app.test_request_context():
        headers = {}
        response = client.post('/get_array_list', headers=headers, json={})
        assert response.status_code == 501
        data = json.loads(response.data)
        assert data["msg"] == "POST No implementado."