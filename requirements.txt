blinker==1.6.2
click==8.1.3
coverage==7.2.3
Flask==2.3.1
Flask-Cors==3.0.10
iniconfig==2.0.0
itsdangerous==2.1.2
Jinja2==3.1.2
MarkupSafe==2.1.2
packaging==23.1
pluggy==1.0.0
pytest==7.3.1
pytest-cov==4.0.0
six==1.16.0
Werkzeug==2.3.0
