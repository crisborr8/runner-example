import xml.etree.ElementTree as ET
#########################################################
#########################################################
#########################################################
coverage_xml = 'coverage.xml'
tree = ET.parse(coverage_xml)
root = tree.getroot()
lines_covered = int(root.attrib['lines-covered'])
lines_valid = int(root.attrib['lines-valid'])
coverage_percent = round(lines_covered*100/lines_valid, 2)
#########################################################
#########################################################
#########################################################
print(f'Porcentaje de cobertura: {coverage_percent}%')
if coverage_percent < 75:
    raise ValueError("La cobertura es menor al 75%. Se debe tomar una acción.")